//
//  ViewController.swift
//  Geocoding
//
//  Created by Gerstmajer Mónika on 2020. 01. 05..
//  Copyright © 2020. Gerstmajer Mónika. All rights reserved.
//
import MapKit
import UIKit
import CoreLocation

class CoordinateViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var distanceView: UIView!
    
    private let locationManager: CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        latitudeTextField.delegate = self
        longitudeTextField.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        
        mapView.showsUserLocation = true
        
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        searchCoordinate()
    }
    
    
    func searchCoordinate() {
        
        guard Double(self.latitudeTextField.text ?? "") != nil && Double(self.longitudeTextField.text ?? "") != nil else {
            
            performShakeAnimation(textField: latitudeTextField)
            performShakeAnimation(textField: longitudeTextField)
            self.showAlert(errorMessage: "Add meg a koordinátákat!")
            return
            
        }
        
        guard let latitude: Double = Double(self.latitudeTextField.text ?? "") else {
            
            performShakeAnimation(textField: latitudeTextField)
            self.showAlert(errorMessage: "Szélességi fok hiányzik!")
            return
            
        }
        
        guard let longitude: Double = Double(self.longitudeTextField.text ?? "") else {
            
            performShakeAnimation(textField: longitudeTextField)
            self.showAlert(errorMessage: "Hosszúsági fok hiányzik!")
            return
            
        }
        
        UIView.animate(withDuration: 0.3) {
            self.searchButton.alpha = 0.0
        }
        
        activityIndicator.startAnimating()
        
        let coordinate = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(coordinate) { (placemarks, error) in
            
            if let placemarks = placemarks {
                
                guard let placemark = placemarks.first else { return }
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = coordinate.coordinate
                annotation.title = placemark.name
                
                self.mapView.addAnnotation(annotation)
                
                self.mapView.setCenter(coordinate.coordinate, animated: true)
                    
                self.showDistanceBetweenCoordinates(coordinate: coordinate)
                
                self.resetSearchButton()
                
            }
            
            if let error = error {
                
                self.showAlert(errorMessage: error.localizedDescription)
                
                self.resetSearchButton()
                
            }
            
        }
        
    }
    
    func checkAuthorizationStatus() -> Bool {
        
        CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        
    }
    
    func showDistanceBetweenCoordinates(coordinate: CLLocation) {
        
        guard checkAuthorizationStatus() else {
            
            distanceView.isHidden = true
            return
            
        }
        
        guard let userLocation = locationManager.location else {
            
            distanceView.isHidden = true
            return
            
        }
        
        if distanceView.isHidden {
            distanceView.isHidden = false
        }
        
        let distanceInKilometer = userLocation.distance(from: coordinate) / 1000
        let formattedDistanceInKilometer = String(format: "%.2f", distanceInKilometer)
        
        distanceLabel.text = "\(formattedDistanceInKilometer) km távol tőled."
        
        if distanceInKilometer > 200 {
            distanceLabel.textColor = .red
        } else {
            distanceLabel.textColor = .green
        }
        
    }
    
    func showAlert(errorMessage: String) {
        
        let alertController = UIAlertController(title: "Hiba", message: errorMessage, preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okButton)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func performShakeAnimation(textField: UITextField) {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        textField.layer.add(animation, forKey: "shake")
        
    }
    
    func resetSearchButton() {
        
        UIView.animate(withDuration: 0.3) {
            self.searchButton.alpha = 1.0
        }
        self.activityIndicator.stopAnimating()
        
    }
    
}


extension CoordinateViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        view.endEditing(true)
    }
    
}

extension CoordinateViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == longitudeTextField {
            latitudeTextField.becomeFirstResponder()
        } else {
            searchCoordinate()
            textField.resignFirstResponder()
        }
        return true
        
    }
    
}
