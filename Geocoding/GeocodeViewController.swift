//
//  GeocodeViewController.swift
//  Geocoding
//
//  Created by Gerstmajer Mónika on 2020. 01. 06..
//  Copyright © 2020. Gerstmajer Mónika. All rights reserved.
//
import MapKit
import UIKit
import CoreLocation

class GeocodeViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    private let locationManager: CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        searchTextField.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        
        mapView.showsUserLocation = true
        
    }
    
    @IBAction func searchButtonTapped(_ sender: Any) {
        searchGeocode()
    }
    
    func searchGeocode() {
        
        guard (searchTextField.text ?? "") != ("") else {
            
            performShakeAnimation()
            self.showAlert(errorMessage: "A kereső mező üres!")
            return
            
        }
        
        UIView.animate(withDuration: 0.3) {
            self.searchButton.alpha = 0.0
        }
        
        activityIndicator.startAnimating()
        
        CLGeocoder().geocodeAddressString(searchTextField.text ?? "") { (placemarks, error) in
            if let placemarks = placemarks {
                
                guard let placemark = placemarks.first else { return }
                guard let location = placemark.location else { return }
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = location.coordinate
                annotation.title = placemark.name
                
                self.mapView.addAnnotation(annotation)
                
                self.mapView.centerCoordinate = location.coordinate
                
                self.showDistanceBetweenCoordinates(coordinate: location)
                
                self.resetSearchButton()
                
            }
            
            if let error = error {
                
                self.showAlert(errorMessage: error.localizedDescription)
                
                self.resetSearchButton()
                
            }
        }
        
    }
    
    func checkAuthorizationStatus() -> Bool {
        
        CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        
    }
    
    func showDistanceBetweenCoordinates(coordinate: CLLocation) {
        
        guard checkAuthorizationStatus() else {
            
            distanceView.isHidden = true
            return
            
        }
        
        guard let userLocation = locationManager.location else {
            
            distanceView.isHidden = true
            return
            
        }
        
        if distanceView.isHidden {
            distanceView.isHidden = false
        }
        
        let distanceInKilometer = userLocation.distance(from: coordinate) / 1000
        let formattedDistanceInKilometer = String(format: "%.2f", distanceInKilometer)
        
        distanceLabel.text = "\(formattedDistanceInKilometer) km távol tőled."
        
        if distanceInKilometer > 200 {
            distanceLabel.textColor = .red
        } else {
            distanceLabel.textColor = .green
        }
        
    }
    
    func showAlert(errorMessage: String) {
    
        let alertController = UIAlertController(title: "Hiba", message: errorMessage, preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okButton)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func performShakeAnimation() {
        
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        searchTextField.layer.add(animation, forKey: "shake")
        
    }
    
    func resetSearchButton() {
        
        UIView.animate(withDuration: 0.3) {
            self.searchButton.alpha = 1.0
        }
        self.activityIndicator.stopAnimating()
        
    }
    
}

extension GeocodeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        view.endEditing(true)
    }
    
}

extension GeocodeViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        searchGeocode()
        
        textField.resignFirstResponder()
        return true
        
    }
    
}
